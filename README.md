## **HERRAMIENTAS DE PROGRAMACION WEB** ##
Trabajando con el DOM, haciendo uso de JavaScript e interactuando con HTML. 

### Descripcion: ###

Esta practica se trabajó con la API DOM, JavaScrip y HTML. La funcion "imprimir" en JavaScrip tiene como objetivo principal obtener el promedio de las calificaciones definidas en una tabla HTML, ademas también se obtiene el nombre, clave y calificación de la materia con mayor y menor calificación. El resultado de dicho procesamiento se mostrará en el documento original HTML.

### Datos: ###
* **Alumno:** Cayetano Herrera Luis Ricardo
* **N/C:** 10161558
* **Carrera:** Ing. Sistemas Computacionales
* **Materia:** Herramientas de Programación Web
* **Grupo:** ISB