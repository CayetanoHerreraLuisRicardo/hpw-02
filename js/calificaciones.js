//============================================================================================INICIO============================================================================================//
function imprimir(){
	//Variables :)
	var tabla= document.getElementById('tcalif'),pPromedio=document.getElementById('promedio'),liMayor= document.getElementById('materiaMayor'),liMenor=document.getElementById('materiaMenor');
	var body=tabla.lastElementChild, longitud=body.children.length;
	var total=0,promedio=0,i=0,cmayor=0,cmenor=0,pmayor=0,pmenor=0,cactual=0;
	var nomMayor=undefined,claMayor=undefined,caMayor=undefined,nomMenor=undefined,claMenor=undefined,caMenor=undefined;
	//Se guarda la suma de las calificaciones
	for(i=0;i<longitud;i++){
		total+= Number(body.children[i].children[2].textContent);
	}
	
	//Se guarda el promedio
	promedio= total/longitud;
	
	//Encuentra la mayor y menor calificacion guardando su índice correspondiente
	for(i=0;i<longitud;i++){
		cactual=Number(body.children[i].children[2].textContent);
		if(i===0){
			pmayor=i;
			pmenor=i;
			cmayor=cactual;
			cmenor=cactual;
		}
		if(cactual>cmayor){
			cmayor=cactual;
			pmayor=i;
		}
		if(cactual<cmenor){
			cmenor=cactual;
			pmenor=i;
		}
	}
	
	//Se obtine el nombre y clave correspondiente al indice con la mayor calificación.
	claMayor=body.children[pmayor].children[0].textContent;
	nomMayor=body.children[pmayor].children[1].textContent;
	caMayor=body.children[pmayor].children[2].textContent;
	
	//Se obtine el nombre y clave correspondiente al indice con la mayor calificación.
	claMenor=body.children[pmenor].children[0].textContent;
	nomMenor=body.children[pmenor].children[1].textContent;
	caMenor=body.children[pmenor].children[2].textContent;
	
	//Mostrar los resultados en los elementos "li" correspondientes
	pPromedio.textContent="Promedio: " +promedio;
	liMayor.textContent="La Materia con nombre "+nomMayor+" y clave "+claMayor+" fue la materia con mayor calificación("+caMayor+")";
	liMenor.textContent="La Materia con nombre "+nomMenor+" y clave "+claMenor+" fue la materia con menor calificación("+caMenor+")";
}